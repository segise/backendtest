/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Database.ChannelDB;
import Entities.Channel;
import Entities.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControllerInChannel", urlPatterns = {"/inChannel"})
public class ControllerInChannel extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String idChan = request.getParameter("parentChannel");
        int idChannel = Integer.parseInt(idChan);

        Channel selectedChannel = new Channel();
        ChannelDB chdb = new ChannelDB();
        ArrayList<Channel> listSubchannels = new ArrayList<>();
        ArrayList<File> listContent = new ArrayList<>();

        selectedChannel.setIdChannel(idChannel);

        try {

            listSubchannels = chdb.listSubchannels(selectedChannel);
            listContent = chdb.contentList(selectedChannel);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (listSubchannels.isEmpty()) {
            request.setAttribute("listInside", listContent);
            RequestDispatcher rd = request.getRequestDispatcher("ElClubDeLaComedia.jsp");
            rd.forward(request, response);
        } else {
            request.setAttribute("listInside", listSubchannels);
            RequestDispatcher rd = request.getRequestDispatcher("series.jsp");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
