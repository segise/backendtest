package Database;

import Entities.Channel;
import java.sql.Connection;
import Database.ConnectionDB;
import Entities.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ChannelDB {

    Connection con;
    PreparedStatement ps;

    //Listing subchannels in a Channel
    public ArrayList<Channel> listSubchannels(Channel ch) throws SQLException {
        con = ConnectionDB.conexion();

        ArrayList<Channel> subchannels = new ArrayList<>();

        ps = con.prepareStatement("SELECT * FROM channels ch "
                + "WHERE ch.parentChannel = ? AND ch.idChannel != ?");
        ps.setInt(1, ch.getIdChannel());
        ps.setInt(2, ch.getIdChannel());

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            subchannels.add(new Channel(
                    rs.getInt("idChannel"),
                    rs.getString("title"),
                    rs.getString("picture")));
        }
        ps.close();
        con.close();
        return subchannels;

    }

    //Display Content
    public ArrayList<File> contentList(Channel ch) throws SQLException {
        con = ConnectionDB.conexion();

        ArrayList<File> listContents = new ArrayList<>();

        ps = con.prepareStatement("Select fi.filePath, md.metadata as metadata from files fi"
                + " join metadata md on md.idContent = fi.idContent" 
                + " join contents con on con.idContent=fi.idContent" 
                + " join channels_contents cc on con.idContent=cc.idContent" 
                + " join channels ch on cc.idChannel=ch.idChannel" 
                + " where ch.idChannel = ?");
        ps.setInt(1, ch.getIdChannel());

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            listContents.add(new File(
                    rs.getString("filePath"),
                    rs.getString("metadata")));
        }
        ps.close();
        con.close();
        return listContents;

    }

    public void seeChannel(Channel ch) throws SQLException {
        con = ConnectionDB.conexion();

        ps = con.prepareStatement("SELECT * FROM channels WHERE title = ?");
        ps.setString(1, ch.getTitle());

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            ch.setIdChannel(rs.getInt("idChannel"));
            ch.setTitle(rs.getString("title"));
            ch.setPicture(rs.getString("picture"));
            ch.setParentChannel(rs.getInt("parentChannel"));
        }

        ps.close();
        con.close();
    }

    public ArrayList<Channel> listChannels() throws SQLException {
        con = ConnectionDB.conexion();

        ArrayList<Channel> listChannels = new ArrayList<>();

        ps = con.prepareStatement("SELECT * FROM channels WHERE parentChannel = idChannel");

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            listChannels.add(new Channel(
                    rs.getInt("idChannel"),
                    rs.getString("title"),
                    rs.getString("picture")));

        }
        ps.close();
        con.close();
        return listChannels;
    }

}
