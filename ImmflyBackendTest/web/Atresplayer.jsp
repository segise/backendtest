<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Entities.Channel"%>
<%@page import="Database.ChannelDB"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
            crossorigin="anonymous"
            />
        <link rel="stylesheet" href="style.css" />
        <title>Atresplayer</title>
    </head>
    <body>
        <header>
            <nav class="navbar-nav">
                <div class="pos-f-t">
                    <div class="collapse" id="navbarToggleExternalContent"></div>
                    <nav class="navbar navbar-light">
                        <button
                            class="navbar-toggler"
                            type="button"
                            data-toggle="collapse"
                            data-target="#toggleContent"
                            >
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="navbar-collapse collapse" id="toggleContent">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="start">Home</a>
                                </li>
                                <li>
                                    <a href="./series.jsp">Series</a>
                                </li>
                                <li>
                                    <a href="">Musica</a>
                                </li>
                                <li>
                                    <a href="">Prensa & Revistas</a>
                                </li>
                                <li>
                                    <a href="">Pel&iacute;culas</a>
                                </li>
                                <li>
                                    <a href="">Ni&ntilde;os</a>
                                </li>
                                <li>
                                    <a href="">Gu&iacute;as</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav navbar-nav navbar-center">
                            <span class="navbar-text">
                                Atresplayer
                            </span>
                        </div>
                    </nav>
                </div>
            </nav>
        </header>
        <article>
            <section>
                <div class="container">
                    <form action="inChannel" method="post">
                        <div class="row text-center">
                            <c:forEach items="${listInside}" var="item" begin="0" end="1"> 
                                <div class="col">
                                    <a href="" id="linked2">
                                        <img src="./images/<c:out value= "${item.picture}"/>.png" class="rounded"/>
                                    </a>
                                    <input type = "submit" value = "${item.idChannel}" name="parentChannel" />
                                </div>
                            </c:forEach>

                        </div>
                        <div class="row text-center">
                            <c:forEach items="${listInside}" var="item" begin="2" end="3"> 
                                <div class="col">
                                    <a href="" id="linked2">
                                        <img src="./images/<c:out value= "${item.picture}"/>.png" class="rounded"/>
                                    </a>
                                    <input type = "submit" value = "${item.idChannel}" name="parentChannel" />
                                </div>
                            </c:forEach>
                        </div>
                    </form>
                </div>
            </section>
        </article>
    </body>
    <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"
    ></script>
</html>
